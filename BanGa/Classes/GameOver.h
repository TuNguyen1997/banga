#ifndef GameOver_
#define GameOver_

#include "cocos2d.h"
USING_NS_CC;

class GameOver : public cocos2d::LayerColor
{
public:
	GameOver() :_label(NULL) {};
	virtual ~GameOver();
	virtual bool init();

	CREATE_FUNC(GameOver);

	void gameOverDone();

	CC_SYNTHESIZE_READONLY(cocos2d::LabelTTF*, _label, Label);

};

class GameOver : public cocos2d::Scene
{
public:
	GameOver() :_layer(NULL) {};
	~GameOver();
	virtual bool init();
	CREATE_FUNC(GameOver);

	CC_SYNTHESIZE_READONLY(GameOver*, _layer, Layer);

};

#endif // GameOver_