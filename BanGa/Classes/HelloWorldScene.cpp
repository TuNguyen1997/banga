﻿
#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "AppDelegate.h"
#include "SimpleAudioEngine.h"
#include "GameOverScene.h"
USING_NS_CC;
using namespace CocosDenshion;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();
	scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_NONE);
	scene->getPhysicsWorld()->setGravity(Vect(0.0f, 0.0f));
	auto layer = HelloWorld::create();
	scene->addChild(layer);
	return scene;
}

bool HelloWorld::init()
{
	
	if (!Layer::init())
	{
		return false;
	}
	auto rootNode = CSLoader::getInstance()->createNode("Screen.csb");//

	rootNode->setPosition(Vec2(0, 0));

	ui::Helper::doLayout(rootNode);
	this->addChild(rootNode);
	Sprite_2 = (Sprite*)rootNode->getChildByName("Sprite_2");
	Sprite_2->setVisible(false);
	Size winSize = Director::getInstance()->getWinSize();
	MayBay = Sprite::create("ship.png");
	auto quay = RotateBy::create(0.0f, -90.0f);
	MayBay->runAction(quay);
	MayBay->setScale(2);
	MayBay->setPosition(Point(Sprite_2->getPosition().x,Sprite_2->getPosition().y+30));
	auto MayBayBody = PhysicsBody::createCircle(MayBay->getContentSize().width/2);
	MayBay->setTag(1);
	MayBayBody->setContactTestBitmask(0x1);
	MayBayBody->setDynamic(false);
	MayBay->setPhysicsBody(MayBayBody);

	this->addChild(MayBay, 1);
	//tao lable
	__String *temSore = __String::createWithFormat("Diem:%i", score);
	lable = Label::createWithTTF(temSore->getCString(), "fonts/arial.ttf", 18);
	lable->setPosition(Point(240,460));
	lable->setColor(Color3B::YELLOW);
	this->addChild(lable);
	//
	this->schedule(schedule_selector(HelloWorld::gameLogic), 0.8);
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(true);
	listener1->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	listener1->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
	listener1->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
	dispatcher->addEventListenerWithSceneGraphPriority(listener1, this);
	auto contactListenner = EventListenerPhysicsContact::create();
	contactListenner->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListenner, this);
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("background-music-aac.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("background-music-aac.wav", true);// True = lặp lại vô hạn
	this->schedule(schedule_selector(HelloWorld::tick),0);

	return true;
}
void HelloWorld::gameLogic(float dt) {
	this->addTarget();

}
bool HelloWorld::onTouchBegan(Touch* touch, Event* event) {
	return true;
}
void HelloWorld::onTouchMoved(Touch* touch, Event* event) {
	Point touchLocation = touch->getLocation();
	//MayBay->setPositionX(touchLocation.x);
	//MayBay->setPositionY(touchLocation.y);
}

void HelloWorld::onTouchEnded(Touch* touch, Event* event) {
	Point location = touch->getLocationInView();
	location = Director::getInstance()->convertToGL(location);
	Size winSize = Director::getInstance()->getWinSize();
	auto dan = Sprite::create("missile.png");
	auto quay = RotateBy::create(0.0f, -90.0f);
	dan->runAction(quay);
	dan->setScale(1);
	dan->setPosition(Point(MayBay->getPosition().x, MayBay->getPosition().y));
	auto danBody = PhysicsBody::createCircle(dan->getContentSize().width / 2);
	dan->setTag(3);
	danBody->setContactTestBitmask(0x1);
	dan->setPhysicsBody(danBody);
	int offx = location.x - dan->getPosition().x;
	int offy = location.y - dan->getPosition().y;

	if (offy <= 0) return;
	this->addChild(dan, 1);

	int realy = winSize.height + (dan->getContentSize().height / 2);

	float ratio = (float)offx / (float)offy;

	int realx =((realy- dan->getContentSize().height)*ratio) + dan->getPosition().x ;
	auto realDest = Point(realx,realy);
	//Chiều dài đường đi của viên đạn, tính theo Pitago a*a = b*b + c*c, a là cạnh huyền tam giác vuông
	int offRealX = realx - dan->getPosition().x;
	int offRealY = realy - dan->getPosition().y;
	float length = sqrtf((offRealX * offRealX) + (offRealY*offRealY));

	// Thiết lập vận tốc 480pixels/1giây
	float velocity = 480 / 1;

	// Thời gian bay của đạn = quãng đường đạn bay chia vận tốc ở trên
	float realMoveDuration = length / velocity;

	// Di chuyển viên đạn tới điểm cuối với thời gian, và tọa độ đã tính ở trên. Khi qua viền màn hình thì biến mất
	dan->runAction(Sequence::create(
		MoveTo::create(realMoveDuration, realDest),
		CallFuncN::create(CC_CALLBACK_1(HelloWorld::RemoveSprite, this)), NULL));
	//
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("pew-pew-lei.wav");

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("pew-pew-lei.wav");


}
bool HelloWorld::onContactBegin(PhysicsContact& contact) {
	//
	auto bullet = (Sprite*)contact.getShapeA()->getBody()->getNode();
	int tag = 0;
	int tag1 = 0;
	if (bullet)
	{
		tag = bullet->getTag();

		auto target = (Sprite*)contact.getShapeB()->getBody()->getNode();
		
		if (target) {
			tag1 = target->getTag();
		}

		if ((tag == 2 & tag1 == 3) || (tag == 3 & tag1 == 2)) {
			bullet->removeFromParentAndCleanup(true);
			target->removeFromParentAndCleanup(true);
			++score;
			__String *temSore = __String::createWithFormat("Diem:%i", score);
			lable->setString(temSore->getCString());
		}
	}
	
	if ((tag == 1 & tag1 == 2) || (tag == 2 & tag1 == 1)) {
		auto gameOverScene = GameOverScene::create(); // Tạo 1 Scene Over của lớp GameOverScene
		gameOverScene->getLayer()->getLabel()->setString("You Lose :["); // Đặt 1 dòng thông báo lên màn hình
		Director::getInstance()->replaceScene(gameOverScene); // Thay thế game Scene =  game Over Scene 
	}
	return true;
}
void HelloWorld::RemoveSprite(Node* sender) {
	auto sprite = (Sprite*)sender;
	this->removeChild(sprite, true);
}
void HelloWorld::addTarget() {
	//tạo aminotion
	SpriteBatchNode *spriteNode = SpriteBatchNode::create("GaGame.png");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("GaGame.plist");
	auto ga = Sprite::createWithSpriteFrameName("ga1.png");
	auto animate = Animate::create(HelloWorld::AnotionGa());
	animate->retain();
	ga->runAction(RepeatForever::create(animate));

	/*SpriteFrameCache::getInstance()->addSpriteFramesWithFile("GaGame.plist", "GaGame.png");

	const int numberSprite = 5;

	auto ga = Sprite::createWithSpriteFrameName("ga1.png");

	Vector<SpriteFrame*> animFrames;
	animFrames.reserve(numberSprite);
	animFrames.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName("ga2.png"));
	animFrames.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName("ga3.png"));
	animFrames.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName("ga4.png"));
	animFrames.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName("ga5.png"));
	Animation* animation = Animation::createWithSpriteFrames(animFrames, 0.1f);
	Animate* animate = Animate::create(animation);

	ga->runAction(RepeatForever::create(animate));
	ga->setFlippedX(TRUE);*/

		ga->setScale(1);
		Size winSize = Director::getInstance()->getWinSize();
		//Tính đoạn xuất hiện quái ko bị ẩn dưới màn hình
		int minX = ga->getContentSize().width / 2;
		int maxX = winSize.width - ga->getContentSize().width / 2;
		//Khoan ma ga duoc phep xuat hien
		int rangeX = maxX - minX;
		int actualX = (rand() % rangeX) + minX;

		ga->setPosition(Point(actualX, winSize.height + (ga->getContentSize().height / 2)));
		auto gabody = PhysicsBody::createCircle(ga->getContentSize().height / 2);
		ga->setTag(2);
		gabody->setContactTestBitmask(0x1);
		ga->setPhysicsBody(gabody);
		this->addChild(ga, 1);
		
	
		//Tính toán tốc độ di chuyển của quái
		int minDuration = (int)2.0;
		int maxDuration = (int)4.0;
		int rangeDuration = maxDuration - minDuration;
		int actualDuration = (rand() % rangeDuration)
			+ minDuration;
		// Di chuyển quái với 1 tốc độ nằm trong khoảng actualDuration , từ điềm xuất hiện tới điểm Point(0,y)

		auto actionMove = MoveTo::create((float)actualDuration, Point(actualX, 0 - ga->getContentSize().height / 2));

		// Kết thúc việc di chuyển của quái khi đã tới điểm cuối
		auto actionMoveDone = CallFuncN::create(CC_CALLBACK_1(HelloWorld::RemoveSprite, this));

		// Chạy 2 Action trên 1 cách tuần tự = lệnh Sequence sau
		ga->runAction(Sequence::create(actionMove, actionMoveDone, NULL));

}
void HelloWorld::tick(float dt)
{

	// 1 biến bool xác nhận Win game ban đầu gán = true;
	bool isWin = false;
	if (score >100) {
		isWin = true;
	}
	if (isWin == true)
	{
		auto gameOverScene = GameOverScene::create();
		gameOverScene->getLayer()->getLabel()->setString("You Win!");
		Director::getInstance()->replaceScene(gameOverScene);
	}
}

cocos2d::Animation *HelloWorld::AnotionGa() {
	Vector<SpriteFrame*> animfamres;
	const int numberSprite = 5;

	for (int i = 2; i <= numberSprite; i++) // Lặp để đọc 8 ảnh trong pak
	{
		// Đọc vào chuỗi str tên file thứ i
		// Tạo 1 khung, lấy ra từ bộ đệm SpriteFrameCache với tên = name
		std::string name = StringUtils::format("ga%d.png", i);
		auto frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
		animfamres.pushBack(frame);// Nhét vào vector

	}
	auto animotion = Animation::createWithSpriteFrames(animfamres, 0.1f);
	return animotion;
}