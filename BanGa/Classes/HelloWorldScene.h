#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

USING_NS_CC;

class HelloWorld : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	Sprite* Sprite_2;
	Sprite* MayBay;
	PhysicsWorld* m_world;
	Label* lable;
	int score;
	void setPhyWorld(PhysicsWorld* world) { m_world = world; };
	void gameLogic(float dt);
	bool onTouchBegan(Touch* touch, Event* event);
	void onTouchMoved(Touch* touch, Event* event);
	void onTouchEnded(Touch* touch, Event* event);
	bool onContactBegin(PhysicsContact& contact);
	void RemoveSprite(Node* sender);
	void addTarget();
	void tick(float dt);
	cocos2d::Animation *AnotionGa();
	CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
